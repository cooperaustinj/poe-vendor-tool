; #Ifwinactive, Path of Exile
^RButton::
	oldClip = %clipboard%
	clipboard = 
	Send ^c
	ClipWait, 0.01
	if(clipboard){
		value := runScript(clipboard)
		; 0 is sell
		if(%value% = 0) {
			Send ^{Click}
		} else {
			SoundPlay transfer.mp3
		}
	}
	clipboard = %oldClip%
Return

runScript(item) {
	RunWait, rubyw PoEVendorTool.rb "%item%", , UseErrorLevel
	return %ErrorLevel%
}
