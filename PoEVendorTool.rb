BASETYPEMAPCSV = 'basetype-mapping.csv'
MODCVS = 'mods.csv'
VENDORFILTERTXT = 'vendor-filter.txt'
DEBUG = true
start = Time.now unless !DEBUG

if DEBUG
	$log = File.open('log.txt', 'a')
	$log.puts "---------OPEN LOG----------"
	at_exit do
		$log.puts "RUNTIME: " + (Time.now - start).to_s
		$log.puts "--------CLOSE LOG--------\n\n"
		$log.close
	end
end
	
def getBaseType(base)
	baseType = []
	File.readlines(BASETYPEMAPCSV).each do |line|
		line = line.strip.downcase
		lineSplit = line.split(',')
		if lineSplit[0] == base
			lineSplit[1..lineSplit.length-1].each do |s|
				baseType += [s]
			end
			return baseType
		end
	end
	return baseType
end

def parseItem(itemPaste)
	item = {}
	
	# Rarity
	item[:rarity] = itemPaste[/rarity: (.*?)\n/, 1]
	if(item[:rarity] != 'rare')
		$log.puts "Item is not rare.  Exiting." unless !DEBUG
		exit 1
	end

	if itemPaste[/map tier: \d+\n/] != nil
		$log.puts "Item is a map.  Exiting." unless !DEBUG
		exit 1
	end	
	if itemPaste[/place into an allocated jewel socket on the passive skill tree./]
		$log.puts "Item is a jewel.  Exiting." unless !DEBUG
		exit 1
	end	
	if itemPaste[/unidentified/]
		$log.puts "Item is unidentified.  Exiting." unless !DEBUG
		exit 1
	end
	if itemPaste[/corrupted/]
		$log.puts "Item is corrupted.  Exiting." unless !DEBUG
		exit 1
	end

	# Name
	item[:name] = itemPaste[/rarity: .*\n(.*?)\n/, 1]
	# Base
	item[:base] = itemPaste[/rarity: .*\n.*\n(.*?)\n/, 1]
	# Base Type
	item[:baseType] = getBaseType(item[:base])
	# Mods
	item[:mods] = {}
	# Quality
	item[:mods]["quality"] = itemPaste[/quality: \+([0-9]+?)%.*\n/, 1].to_f
	# Armour
	totalAr = itemPaste[/armour: (\d+).*\n/, 1]
	if totalAr != nil
		item[:mods]["totalar"] = totalAr.to_f
	end
	# Evasion
	totalEv = itemPaste[/evasion: (\d+).*\n/, 1]
	if totalEv != nil
		item[:mods]["totalev"] = totalEv.to_f
	end
	# Energy Shield
	totalEs = itemPaste[/energy shield: (\d+).*\n/, 1]
	if totalEs != nil
		item[:mods]["totales"] = totalEs.to_f
	end
	# Critical Strike Chance
	critChance = itemPaste[/critical strike chance: (\d+(\.\d+)?)\n/, 1]
	if critChance != nil
		item[:mods]["critchance"] = critChance.to_f
	end
	# Attacks per Second
	aps = itemPaste[/attacks per second: (\d+(\.\d+)?).*\n/, 1]
	if aps != nil
		item[:mods]["aps"] = aps.to_f
	end
	# Physical DPS
	pdpsmatch = itemPaste.match(/physical damage: (\d+)-(\d+).*\n/)
	if pdpsmatch != nil
		minPDmg, maxPDmg = pdpsmatch.captures
		item[:mods]["pdps"] = (minPDmg.to_f + maxPDmg.to_f) / 2 * aps.to_f
	end
	# Elemental DPS
	elementalDamage = itemPaste[/elemental damage: .*\n/]
	if elementalDamage != nil
		eleValues = elementalDamage.scan(/(\d+)/)
		totalEleValues = 0
		eleValues.each do |v|
			totalEleValues += v[0].to_f
		end
		item[:mods]["edps"] = totalEleValues / 2 * aps.to_f
	end
	# Chaos DPS
	chaosdpsmatch = itemPaste.match(/chaos damage: (\d+)-(\d+).*\n/)
	if chaosdpsmatch != nil
		minCDmg, maxCDmg = chaosdpsmatch.captures
		item[:mods]["chdps"] = (minCDmg.to_f + maxCDmg.to_f) / 2 * aps.to_f
	end
	
	File.readlines(MODCVS).each do |affixLine|
		regex = Regexp.new(affixLine.split(',')[1])
		value = itemPaste.scan(regex)
		lineSplit = affixLine.downcase.split(',')
		value.each do |v|
			if(v.length == 1)
				actualValue = v[0].to_f
			else
				actualValue = (v[0].to_f + v[1].to_f) / 2
			end
			if item[:mods][lineSplit[0]] == nil
				item[:mods][lineSplit[0]] = actualValue
			else
				item[:mods][lineSplit[0]] += actualValue
			end
		end
	end
	
	return item
end

# def parseFilter
# 	filter = {}
	
# 	curSection = {}
# 	File.readlines(VENDORFILTERTXT).each do |line|
# 		if(line.strip == "") then next end

# 		line = line.downcase
# 		lineSplit = line.split(' ')
# 		if line.start_with?("\t")
# 			curFilter = {}
# 			curFilter[:value] = lineSplit[1].to_f
# 			curFilter[:weight] = lineSplit[2].to_f
# 			curSection[:filter][lineSplit[0].downcase] = curFilter
# 		else
# 			curSection = {}
# 			curSection[:threshold] = lineSplit[1].to_f
# 			curSection[:filter] = {}
# 			filter[lineSplit[0].downcase] = curSection
# 		end
# 	end
	
# 	return filter
# end

def parseFilter(base, baseType)
	filter = {}
	flag = false
	File.readlines(VENDORFILTERTXT).each do |line|
		if(line.strip == "") then next end
		lineSplit = line.downcase.split(' ')
		
		if flag
			if !line.start_with?("\t")
				break
			end
			cur = {}
			cur[:value] = lineSplit[1].to_f
			cur[:weight] = lineSplit[2].to_f
			if filter[:filter][lineSplit[0]] == nil
				filter[:filter][lineSplit[0]] = [cur]
			else
				filter[:filter][lineSplit[0]] += [cur]
			end
		end
		
		if !flag && (baseType.include?(lineSplit[0]) || lineSplit[0] == base.gsub(' ', ''))
			flag = true
			key = lineSplit[0]
			cur = {}
			cur[:threshold] = lineSplit[1].to_f
			cur[:filter] = {}
			filter = cur
		end
	end
	
	return filter
end

# Parse Item Paste
itemPaste = ARGV[0].downcase.tr("\r", '')
# itemPaste = IO.read('item.txt').downcase
item = parseItem(itemPaste)

# Parse Filter
filter = parseFilter(item[:base], item[:baseType])

if DEBUG
	item.each do |key, val|
		if val != nil
			$log.puts key.to_s.rjust(14) + "\t" + val.to_s.ljust(14) unless !DEBUG
		end
	end
	item[:mods].each do |key, val|
		if val != nil
			$log.puts key.to_s.rjust(14) + "\t" + val.to_s.ljust(14) unless !DEBUG
		end	
	end
	# $log.puts filter unless !DEBUG
end

if filter.length > 0
	section = filter
	threshold = section[:threshold]
	f = section[:filter]
	weight = 0
	
	f.each do |key, val|
		if item[:mods].has_key?(key)
			# puts f[key]
			f[key].each do |subfilter|
				if item[:mods][key] >= subfilter[:value]
					$log.puts key + " of " + item[:mods][key].to_s + " > " + subfilter[:value].to_s + " : " + subfilter[:weight].to_s unless !DEBUG
					weight += subfilter[:weight]
				end
			end
		end
	end
	$log.puts "WEIGHT: #{weight}" unless !DEBUG
	if weight >= threshold
		exit 1
	end
end